#################################
## NAVIGATION MOBILE CONSTANTS ##
#################################

plugin.tx_teufels_cpt_nav_mobile {
	settings {
        lib {
            navigation {
                special = 2
                cache {
                    postfix = default
                }
            }
        }
        production {
            includePath {
                public = EXT:teufels_cpt_nav_mobile/Resources/Public/
                private = EXT:teufels_cpt_nav_mobile/Resources/Private/
                frontend {
                    public = typo3conf/ext/teufels_cpt_nav_mmobile/Resources/Public/
                }

            }
            optional {
                active = 1
            }
        }
    }
}